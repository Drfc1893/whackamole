#pragma once
//library to be included for using texture sprites and fonts 
#include<SFML/Graphics.hpp>
//library for handeling vecotrs for collecting objects
#include <vector>


class Player
{
public: //access level (to be discussed later)

	//constructor 
	Player(sf::Texture& playerTexture, sf::Vector2u screenSize);

	//variables (data members) used by this class 
	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;

	//function to call player-specific code 
	void Input();
	void Update(sf::Time frameTime);

	void Reset(sf::Vector2u screenSize);
};

