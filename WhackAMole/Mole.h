#pragma once
#include <SFML/Graphics.hpp>
#include <vector>//include vector library

//definition of item class
class Mole
{
public:

	//constructor 
	Mole(sf::Texture & moleTextures, sf::Vector2u screenSize);

	//variable (datamemebers) used by this class
	sf::Sprite sprite;
	int pointValue;
};

