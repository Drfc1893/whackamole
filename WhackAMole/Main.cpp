#include <SFML/Graphics.hpp>
//librarys included 
#include <SFML/Graphics.hpp> //library needed for creating and managing SFML windows
#include <SFML/Audio.hpp>//Audio library included 
#include <String>//adds string library 
#include <vector>//adds vectors to program 
#include "Mole.h" // include mole.h for vectors
#include <cstdlib>
#include <time.h>
#include "Player.h"

int main()
{


	//includeds the sfml window file for game 
	sf::RenderWindow gameWindow; //this variable stores integer numbers and characvter text variables 
	//set up teh SFML window, passing the dimensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), " Whack A Mole ", sf::Style::Titlebar | sf::Style::Close);

	//-------------------------------------
	//game setup 
	//-------------------------------------

	sf::Texture playerTexture;
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	//assign sprite and display sprite 
	/*sf::Sprite playerSprite;
	playerSprite.setTexture(playerTexture);*/
	//declare a player object 
	Player playerObject(playerTexture, gameWindow.getSize());

	//create and load in audio to game
	sf::Music gameMusic; //declaring music variable 
	gameMusic.openFromFile("Assets/Audio/music.ogg");//opens file 
	gameMusic.play();//plays music 

	//add text font to game
	sf::Font gameFont;//declaring font 
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");//opens font file 

	//title text
	//using font in game 
	sf::Text titleText;//declares variable of type sf::text
	titleText.setFont(gameFont);//states what text object font should be used 
	titleText.setString(" Whack A Mole ");//sets string 
	titleText.setCharacterSize(24);//size of text 
	titleText.setFillColor(sf::Color::Cyan);//colour of text 
	titleText.setStyle(sf::Text::Bold | sf::Text::Italic);//adds a style/font type to text 
	titleText.setPosition(gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);//seting text object position on screen 


	//author text 
	sf::Text titleText2;//declares variable of type sf::text
	titleText2.setFont(gameFont);//states what text object font should be used 
	titleText2.setString(" James Shields ");//sets string 
	titleText2.setCharacterSize(12);//size of text 
	titleText2.setFillColor(sf::Color::Cyan);//colour of text 
	titleText2.setStyle(sf::Text::Bold | sf::Text::Italic);//adds a style/font type to text 
	titleText2.setPosition(gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 60);//seting text object position on screen 


	//adding gane variables

	int score = 0;//creates integer/wholenumber called score 

	//adding score text 
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString(" Score: " + std::to_string(score));
	scoreText.setCharacterSize(16);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(30, 30);

	//timer code set up 
	sf::Text timerText;
	timerText.setFont(gameFont);
	timerText.setString(" Time Remaining: 0 ");
	timerText.setCharacterSize(16);
	timerText.setFillColor(sf::Color::White);
	timerText.setPosition(gameWindow.getSize().x - timerText.getLocalBounds().width - 30, 30);
	//add time to timer 
	sf::Time timeLimit = sf::seconds(60.0f);//f stands for float also declares variable 
	sf::Time timeReamining = timeLimit;//declares another variable 
	sf::Clock gameClock;//declares a variable type 

	//srand code for random position
	srand(time(NULL));

	//game over text 
	//declare a text valkue called gameover to hold our game over display 
	sf::Text gameOverText;
	//set the font 
	gameOverText.setFont(gameFont);
	//set string of text taht will be displayed 
	gameOverText.setString(" Game Over ");
	//set size in oixels
	gameOverText.setCharacterSize(72);
	//set colour of text 
	gameOverText.setFillColor(sf::Color::Cyan);
	//set text type 
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	//position 
	gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);


	//mole code
	//includeds vectors for random spawning 
	//load all textures that will be used by our moles 
	sf::Texture moleTexture;
	/*moleTexture.push_back(sf::Texture());//0
	moleTexture.push_back(sf::Texture());//1
	moleTexture.push_back(sf::Texture());//2
	moleTexture.push_back(sf::Texture());//3
	moleTexture.push_back(sf::Texture());//4
	moleTexture.push_back(sf::Texture());//5
	moleTexture.push_back(sf::Texture());//6
	moleTexture.push_back(sf::Texture());//7
	moleTexture.push_back(sf::Texture());//8*/
	//moleTexture[0].loadFromFile("Assets/Graphics/mole.png");
	//create the vector to hold our Mole 
	std::vector<Mole> moles;

	/*moles.push_back(Mole());//0
	moles.push_back(Mole());//1
	moles.push_back(Mole());//2
	moles.push_back(Mole());//3
	moles.push_back(Mole());//4
	moles.push_back(Mole());//5
	moles.push_back(Mole());//6
	moles.push_back(Mole());//7
	moles.push_back(Mole());//8*/

	moleTexture.loadFromFile("Assets/Graphics/mole.png");
	
	/*moles[0].sprite.setTexture(moleTexture);
	moles[0].pointValue = 100;
	moles[0].sprite.setPosition(200, 200);
	*/
	//load up a few random starting moles - 1 of each type 
	//moles.push_back(Mole(moleTexture, gameWindow.getSize()));
	
	//Mole[0].Texture.setPosition(200, 200);
	/*moleTexture[1].sprite.setPosition(-100, 100);
	moleTexture[2].sprite.setPosition(100, -100);
	moleTexture[3].sprite.setPosition(-200, 200);
	moleTexture[4].sprite.setPosition(200, -100);
	moleTexture[5].sprite.setPosition(-100, 100);
	moleTexture[6].sprite.setPosition(-100, 100);
	moleTexture[7].sprite.setPosition(-100, 100);
	moleTexture[8].sprite.setPosition(-100, 100);*/
	//Mole timer for spawning 
	//create a time value to score the total time between each Mole spawn
	sf::Time moleSpawnDuration = sf::seconds(2.0f);
	//create a timer to store the time remaining for our game 
	sf::Time moleSpawnRemaining = moleSpawnDuration;

	//include vector2 = maths vector
	sf::Vector2f playerVelocity(0.0f, 0.0f);

	//include speed float
	float speed = 100.0f;


	//load the pick up sound effect file into a soundBuffer 
	sf::SoundBuffer pickupSoundBuffer;
	pickupSoundBuffer.loadFromFile("Assets/Audio/pickup.wav");
	//set up a sound object to play the sound later, and associate it with the soundbuffer 
	sf::Sound pickupSound;
	pickupSound.setBuffer(pickupSoundBuffer);

	//game ending sounds victory 
	sf::SoundBuffer victorySoundBuffer;
	victorySoundBuffer.loadFromFile("Assets/Audio/victory.ogg");
	//set up a sound object to play the sound later , and associate it with the soundBuffer 
	sf::Sound VictorySound;
	VictorySound.setBuffer(victorySoundBuffer);

	//game over variable to track if the game is done 
	bool gameOver = false;


	//GameLoop
	//repeat as long as teh window is open 

	
	while (gameWindow.isOpen())
	{
		//------------------------------------------------------
		//input selection 
		//------------------------------------------------------
		//ToDo: check for input (done)
		//declare a variable to hold an event, called gameEvent 
		sf::Event gameEvent;
		//loop through all events and poll them, poutting eahc one into our ganmeEvent variable 
		while (gameWindow.pollEvent(gameEvent))
		{

			//this section will be repeated for each event waiting to be processed 

			//did the pkayer try to close the window 
			if (gameEvent.type == sf::Event::Closed)
			{
				//if so, call the close function onm the window 
				gameWindow.close();

			}
		}
		//end event polling loop

		//new player code (player keybind inout)
		playerObject.Input();
		
		//check if we should reset teh game 
		if (gameOver && sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			//reset the game 
			score = 0;
			timeReamining = timeLimit;
			moles.clear();
			moles.push_back(Mole(moleTextures, gameWindow.getSize()));
			moles.push_back(Mole(moleTextures, gameWindow.getSize()));
			moles.push_back(Mole(moleTextures, gameWindow.getSize()));
			gameMusic.play();
			gameOver = false;
			playerObject.Reset(gameWindow.getSize());

		}

		//allows player to quite game when gameOver screen shows 
		if (gameOver && sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		{
			//quite game 
			return 0;

		}

		//-------------------------------------
		//update section
		//-------------------------------------
		//ToDo: update game state

		//check if the time has run out 
		if (timeReamining.asSeconds() <= 0)
		{
			//dont let the time go lower than 0 
			timeReamining = sf::seconds(0);
			if (gameOver == false)
			{

				gameOver = true;
				//stop the main music from playign 
				gameMusic.stop();
				//pplay victory sound
				VictorySound.play();
			}

		}

		sf::Time frameTime = gameClock.restart();
		timeReamining = timeReamining - frameTime;
		timerText.setString(" Time Remaining: " + std::to_string((int)timeReamining.asSeconds()));

		//score = score + 1;//adds to score 
		scoreText.setString(" Score: " + std::to_string(score));//updates teh score 

		//only perform this update logic if the game is still running:
		if (!gameOver)
		{

			//update our mole spawn time remaining based on how much time passed last frame
			moleSpawnRemaining = moleSpawnRemaining - frameTime;
			//check if time remaining to nect spawn has reahced 0 
			if (moleSpawnRemaining <= sf::seconds(0.0f))
			{
				//time to spawn a new mole 
				moles.push_back(Mole(moleTextures, gameWindow.getSize()));

				//reset time remainig to full duration 
				moleSpawnRemaining = moleSpawnDuration;
			}



		}

		//update our mole spawn time remaining based on how mcuh time passed last frame 
		moleSpawnRemaining = moleSpawnRemaining - frameTime;
		//check if time remaining to next spawn has reaxched 0
		if (moleSpawnRemaining <= sf::seconds(0.0f))
		{

			//time to spawn a new mole !
			moles.push_back(Mole(moleTextures, gameWindow.getSize()));
			//reset time remaining to full duration 
			moleSpawnRemaining = moleSpawnDuration;

		}

		//add player movemnet in velocity to the update function 
		//playerSprite.setPosition(playerSprite.getPosition() + playerVelocity * frameTime.asSeconds());

		//new player update code 
		//move player 
		playerObject.Update(frameTime);

		//check for collision 
		for (int i = moles.size() - 1; i >= 0; --i)
		{
			sf::FloatRect moleBounds = moles[i].sprite.getGlobalBounds();
			sf::FloatRect playerBounds = playerObject.sprite.getGlobalBounds();

			if (moleBounds.intersects(playerBounds))
			{
				//our player touched the mole!
				score += moles[1].pointValue;
				//removes mole from the vector 
				moles.erase(moles.begin() + i);

				//play pick up sound 
				pickupSound.play();

			}
		}


		//-------------------------------------
		//draw section
		//-------------------------------------
		//ToDo: draw graphics (done)

		//clear the window to a single colour 
		gameWindow.clear(sf::Color::Black);

		//draw everything to the window 
		gameWindow.draw(playerObject.sprite);

		//draw text to the window 
		gameWindow.draw(titleText);
		//author text 
		gameWindow.draw(titleText2);
		//draw score text 
		gameWindow.draw(scoreText);
		//draw timer
		gameWindow.draw(timerText);
		//ToDo : draw more stuff 

		// only draw these moles if game isnt over 
		if (!gameOver)
		{
			gameWindow.draw(playerObject.sprite);

			//draw alll our moles 
			for (int i = 0; i < moles.size(); ++i)
			{
				gameWindow.draw(moles[i].sprite);
			}
		}
		//only if gameover has ended 
		if (gameOver)
		{
			gameWindow.draw(gameOverText);
		}

		//draw vectors/pickups/moles
		for (int i = 0; i < moles.size(); ++i)
		{
			gameWindow.draw(moles[i].sprite);
		}


		//dispaly the window content on the screen
		gameWindow.display();



	}
	//end of loop

	return 0;


}
